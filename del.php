#!/usr/bin/env php
<?php

use Gitlab\Client;
use Gitlab\Model\Project;

require 'vendor/autoload.php';

$target = $argv[1];

// Create a personal token and grant 'api' scope: https://gitlab.com/profile/personal_access_tokens
if (!$token = getenv('GITLAB_TOKEN')) {
    exit('Missing token');
}
$client = Client::create(getenv('GITLAB_URL') ?: 'https://gitlab.com')->authenticate($token, Client::AUTH_OAUTH_TOKEN);
// $project = new Project($target, $client);
$pager = new \Gitlab\ResultPager($client);

// My Gitlab server hands out bad domains so cant use the ->next url directly.
do {
  $issues = $pager->fetch($client->api('issues'), 'all', [$target]);
  $has_next = $pager->hasNext();
  foreach ($issues as $issue) {
      $client->issues()->remove($target, $issue['iid']);
  }
}
while ($has_next);