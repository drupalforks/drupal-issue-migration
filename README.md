# Drupal.org issue migration

- push.php - Migrate into Gitlab
- del.php - Delete all issues in target project (i.e. a re-migrate scenario)

# Run Pipeline

To perform a migration, browse to [Run Pipeline](https://gitlab.com/drupalspoons/drupal-issue-migration/pipelines), leave `master` untouched, and use these variables:

| Name | Value | Notes |
| ------ | ------ | ----- |
| REF_NAME | master |No need to change |
| SLUG | [drupal.org project machine name] | e.g. ctools, pathauto |
